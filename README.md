# Who said text isn't data? Text analysis for data journalists with R

Here you can find all the materials used on my presentation "Who said text isn't data? Text analysis for data journalists with R" on 2020 DataHarvest.

- [Slides](https://ruimgbarros.gitlab.io/tidytext4journalists/slides.html#1)
- [Code used](analysis.Rmd)
- [Eurovision Lyrics Dataset](data/erovision_lyrics.csv)

Eurovision Lyrics were scrapped from [4lyrics.eu](https://4lyrics.eu/) and [wiwibloggs.com](wiwibloggs.com).



The presentation was created using [remark.js](http://remarkjs.com/) and the R package [**xaringan**](https://github.com/yihui/xaringan)

My xaringan theme (using [xaringanthemer](https://pkg.garrickadenbuie.com/xaringanthemer/)):

```
mono_accent(
    base_color         = "#F48024",
    header_font_google = google_font("IBM Plex Sans", "700"),
    text_font_google   = google_font("IBM Plex Sans Condensed"),
    code_font_google   = google_font("IBM Plex Mono")
    )
```



